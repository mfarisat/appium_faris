<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT LOGO -->
<br />
<div align="center">

<h3 align="center">Appium + Selenium + TestNG + Cucumber (Mobile App Testing)</h3>

  <p align="center">
    by M. Faris
    <br />
    <a href="https://gitlab.com/mfarisat/appium_faris/-/issues"><strong>Report Issues »</strong></a>
    <br />
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
        <li><a href="#test-scenarios">Test Scenarios</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#running-the-automation-tests">Running the Automation Tests</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

This is a POC Java project for Automation Testing using Appium to test Google Calculator mobile app. 
The goal of this project is to demonstrate how an Automation Testing is done using tools/frameworks. 
Of course this project is not limited to its current conditions. 
You may clone it to your own environment and customize it further depending on your needs.

* [Get to know about Automation Testing](https://www.techtarget.com/searchsoftwarequality/definition/automated-software-testing)
* [Get to know Appium](https://appium.io/docs/en/latest/)
* [Get to know Selenium](https://www.selenium.dev/)
* [Get to know TestNG](https://testng.org/)
* [Get to know Cucumber](https://cucumber.io/)

<p align="right">(<a href="#readme-top">back to top</a>)</p>


### Built With

* Java 17
* Maven 3
* Appium 9
* Selenium 4
* Cucumber 7
* TestNG 7

<p align="right">(<a href="#readme-top">back to top</a>)</p>


### Test Scenarios

* Google Calculator Add operation
* Google Calculator Subtract operation

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

The following are the instructions on setting up your project locally.
To get a local copy up and running, follow these simple steps.

### Prerequisites

The following prerequisites that you will need to get started:

1. This repo of course! You may download it as a zip file or "git clone" it to your computer.
2. [OpenJDK](https://adoptium.net/temurin/releases/) (I used v17 as a base).
3. An IDE ([Eclipse](https://www.eclipse.org/downloads/) or [IntelliJ IDEA Ultimate/Community Edition](https://www.jetbrains.com/idea/download/?section=windows)).
4. [Node.js](https://nodejs.org/en/download/current)
5. Appium (after installation of Node.js), by running this command in Command Prompt/Terminal.
   ```sh
   npm install -g appium
   ```
6. Android device or emulator ([get Android Studio](https://developer.android.com/studio) and [install emulator](https://developer.android.com/studio/run/emulator)).
7. Enabled "USB debugging" in Android device/emulator. [Refer here on how to do it.](https://developer.android.com/studio/debug/dev-options)
8. Enabled "Install via USB" and "USB debugging (security settings)" if you use Xiaomi devices.
9. Enabled "Disable permission monitoring" if you use Oppo, Realme and OnePlus devices.
10. Installed [Google Calculator app.](https://www.apkmirror.com/apk/google-inc/google-calculator/) Refer [here](https://www.vrtourviewer.com/docs/adb/) on how to install it in your emulator (if you use emulator).

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### Running the Automation Tests

1. Ensure that you have extracted or git-cloned the project folder into your local computer.
2. Open the project folder in your IDE.
3. Look for `local.properties` in `src/test/resources`. Configure it as follows:
   ```sh
   appium.ip=127.0.0.1  # we don't use it for now
   appium.port=4723  # we don't use it for now
   device.identifier=   # put your device ID here. You can get it by running the command "adb devices" in Command Prompt/Terminal when the device/emulator is connected via USB/running.
   app.id=com.google.android.calculator
   app.activity.id=com.android.calculator2.Calculator
   ```
4. Run the tests through the `testng.xml` file.
5. Ensure your Android device/emulator is connected (via USB for real devices)/running. You should be able to see the running tests in your device/emulator.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

M. Faris - [Facebook](https://fb.me/its.me.eff) - mfaris.official@gmail.com

GitHub : [mfarisgh](https://github.com/mfarisgh)

GitLab : [mfarisgl](https://gitlab.com/mfarisgl)

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgements

* [Nabendu Biswas (Waldo)](https://www.waldo.com/blog/using-appium-cucumber)
* [D. Husni Fahri Rizal (Medium)](https://medium.com/ralali-engineering/basic-appium-and-cucumber-bdd-framework-3eabef9ec033)
* [Sourojit Das (BrowserStack)](https://www.browserstack.com/guide/appium-inspector)
* [Avinash Mishra (Inviul)](https://www.inviul.com/auto-start-appium-server/)
* Appium.io * [Link 1](https://discuss.appium.io/t/unable-to-start-appium-service-by-appiumdriverlocalserivce/6324/22?page=2)
* Appium.io * [Link 2](https://appium.io/docs/en/2.0/quickstart/uiauto2-driver/)
* [Android Developers](https://developer.android.com/tools/variables)
* [Tim Lewis (Stack Overflow)](https://stackoverflow.com/questions/2263929/regarding-application-properties-file-and-environment-variable)

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
Feature: Android Calculator

  Background:
    Given User open Calculator app

  @OpenCalc
  Scenario: Adding two positive numbers

    When User adds two numbers
    Then User gets the sum of the numbers

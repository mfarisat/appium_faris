Feature: Android Calculator - Minus Operation

  Background:
    Given User opens Calculator app

  @OpenCalc
  Scenario: Subtracting two positive numbers

    When User subtracts two numbers
    Then User gets the result of the numbers

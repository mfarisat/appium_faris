package com.emeff23.ac.definitions;

import com.emeff23.ac.pomartifacts.actions.CalcActions;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

public class AndroidCalculatorMinusDef {

    private static final Logger LogThis = LogManager.getLogger(AndroidCalculatorDef.class.getName());

    CalcActions calcActions = new CalcActions();

    @Given("User opens Calculator app")
    public void openCalc() {

        LogThis.info("Started Calculator app");

    }

    @When("User subtracts two numbers")
    public void subNumbers() {

        try {

            LogThis.info("Processing minus operation");

            calcActions.subNumbers();

        } catch (Exception e) {

            LogThis.error("Exception e = " + e.getMessage());
            Assert.fail("Exception e = " + e.getMessage());

        }

    }

    @Then("User gets the result of the numbers")
    public void finishOps() {

        try {

            LogThis.info("Comparing result");

            Assert.assertEquals(calcActions.getResult(), "−1");

        } catch (Exception e) {

            LogThis.error("Exception e = " + e.getMessage());
            Assert.fail("Exception e = " + e.getMessage());

        }

        LogThis.info("Operation finished");

    }

}

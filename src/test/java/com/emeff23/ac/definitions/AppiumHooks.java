package com.emeff23.ac.definitions;

import com.emeff23.ac.pomartifacts.common.AppiumHelper;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class AppiumHooks {

    @Before
    public static void setUp() {

        AppiumHelper.setUpDriverCapabilities();

    }

    @After
    public static void tearDown() {

        AppiumHelper.exitApp();

        AppiumHelper.tearDown();

    }

}

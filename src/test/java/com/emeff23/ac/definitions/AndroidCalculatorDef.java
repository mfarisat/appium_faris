package com.emeff23.ac.definitions;

import com.emeff23.ac.pomartifacts.actions.CalcActions;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

public class AndroidCalculatorDef {

    private static final Logger LogThis = LogManager.getLogger(AndroidCalculatorDef.class.getName());

    CalcActions calcActions = new CalcActions();

    @Given("User open Calculator app")
    public void openCalc() {

        LogThis.info("Started Calculator app");

    }

    @When("User adds two numbers")
    public void addNumbers() {

        try {

            LogThis.info("Processing add operation");

            calcActions.addNumbers();

        } catch (Exception e) {

            LogThis.error("Exception e = " + e.getMessage());
            Assert.fail("Exception e = " + e.getMessage());

        }

    }

    @Then("User gets the sum of the numbers")
    public void finishOps() {

        try {

            LogThis.info("Comparing result");

            Assert.assertEquals(calcActions.getResult(), "3");

        } catch (Exception e) {

            LogThis.error("Exception e = " + e.getMessage());
            Assert.fail("Exception e = " + e.getMessage());

        }

        LogThis.info("Operation finished");

    }

}

package com.emeff23.ac.common;

public class CentralVars {

    public static String ProjectPath = System.getProperty("user.dir");

    public static String PropNameAppiumIp = "appium.ip";

    public static String PropNameAppiumPort = "appium.port";

    public static String PropNameDevIdentifier = "device.identifier";

    public static String PropNameAppId = "app.id";

    public static String PropNameAppActivityId = "app.activity.id";

}

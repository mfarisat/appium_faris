package com.emeff23.ac.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public abstract class PropFileMgmt {

    private static final Logger LogThis = LogManager.getLogger(PropFileMgmt.class.getName());

    public static String getPropertyValue(String propName) {

        String propValue = "";
        Properties properties = new Properties();
        try {
            InputStream inputStream = Files.newInputStream(Paths
                    .get(CentralVars.ProjectPath + "/src/test/resources/local.properties"));
            properties.load(inputStream);
            propValue = properties.getProperty(propName);
        } catch (Exception e) {
            LogThis.error("Exception e = " + e.getMessage());
        }

        return propValue;

    }

}

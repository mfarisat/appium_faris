package com.emeff23.ac.pomartifacts.actions;

import com.emeff23.ac.pomartifacts.common.AppiumHelper;
import com.emeff23.ac.pomartifacts.locators.CalcLocators;
import org.openqa.selenium.support.PageFactory;

public class CalcActions {

    CalcLocators calcLocators;

    public CalcActions() {

        this.calcLocators = new CalcLocators();

        PageFactory.initElements(AppiumHelper.getDriver(), calcLocators);

    }

    public void addNumbers() {

        calcLocators.buttonOne.click();

        calcLocators.buttonAdd.click();

        calcLocators.buttonTwo.click();

        calcLocators.buttonEquals.click();

    }

    public void subNumbers() {

        calcLocators.buttonOne.click();

        calcLocators.buttonMinus.click();

        calcLocators.buttonTwo.click();

        calcLocators.buttonEquals.click();

    }

    public String getResult() {

        return calcLocators.result.getText();

    }

}

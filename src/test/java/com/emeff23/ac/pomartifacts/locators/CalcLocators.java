package com.emeff23.ac.pomartifacts.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CalcLocators {

    @FindBy(id = "com.google.android.calculator:id/digit_1")
    public WebElement buttonOne;

    @FindBy(id = "com.google.android.calculator:id/digit_2")
    public WebElement buttonTwo;

    @FindBy(id = "com.google.android.calculator:id/op_add")
    public WebElement buttonAdd;

    @FindBy(id = "com.google.android.calculator:id/op_sub")
    public WebElement buttonMinus;

    @FindBy(id = "com.google.android.calculator:id/eq")
    public WebElement buttonEquals;

    @FindBy(id = "com.google.android.calculator:id/result_final")
    public WebElement result;

}

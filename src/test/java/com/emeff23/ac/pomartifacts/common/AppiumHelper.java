package com.emeff23.ac.pomartifacts.common;

import com.emeff23.ac.common.CentralVars;
import com.emeff23.ac.common.PropFileMgmt;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.service.local.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.URL;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class AppiumHelper {

    private static final Logger LogThis = LogManager.getLogger(AppiumHelper.class.getName());

    private static AppiumDriver driver;

    private static ThreadLocal<AppiumDriver> threadedDriver = new ThreadLocal<>();

    private static AppiumDriverLocalService service;

    private static final String appiumJsPath = "C:\\Users\\mfarisTB\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js";

    private static final String nodePath = "C:\\Program Files\\nodejs\\node.exe";

    public static void setUpDriverCapabilities() {

        try {

            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("platformName", "Android");
            capabilities.setCapability("platformVersion", "11");
            capabilities.setCapability("deviceName", PropFileMgmt.getPropertyValue(CentralVars.PropNameDevIdentifier));
            capabilities.setCapability("automationName", "UiAutomator2");
            capabilities.setCapability("appPackage", PropFileMgmt.getPropertyValue(CentralVars.PropNameAppId));
            capabilities.setCapability("appActivity", PropFileMgmt.getPropertyValue(CentralVars.PropNameAppActivityId));
            capabilities.setCapability("noReset", "true");
            capabilities.setCapability("forceAppLaunch", "true");

            AppiumServiceBuilder serviceBuilder = new AppiumServiceBuilder();

            /*
            service = AppiumDriverLocalService.buildService(serviceBuilder
                    .withAppiumJS(new File(appiumJsPath))
                    .usingDriverExecutable(new File(nodePath))
                    .withLogFile(new File(appiumLogPath))
                    .withIPAddress(PropFileMgmt.getPropertyValue(CentralVars.PropNameAppiumIp))
                    .usingPort(Integer.parseInt(PropFileMgmt.getPropertyValue(CentralVars.PropNameAppiumPort))));

             */

            serviceBuilder.withAppiumJS(new File(appiumJsPath))
                    .usingDriverExecutable(new File(nodePath));

            service = serviceBuilder.build();

            service.start();

            String service_url = service.getUrl().toString();

            LogThis.debug("Appium Service Address = " + service_url);

            driver = new AppiumDriver(new URL(service_url), capabilities);

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        } catch (Exception e) {

            LogThis.error("Exception e = " + e.getMessage());

        }

    }

    public static synchronized AppiumDriver getDriver() {

        if (Objects.nonNull(driver)) {
            threadedDriver.set(driver);
        }
        return threadedDriver.get();

    }

    public static void exitApp() {

        driver.navigate().back();

    }

    public static void tearDown() {

        driver.quit();

        service.stop();

    }

}
